<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter;

use Spinit\Datasource\Core\AdapterInterface;
use Spinit\Datasource\Core\StructInterface;
use Spinit\Datasource\Core\QueryInterface;

use Webmozart\Assert\Assert;

use Spinit\Util;

/**
 * Description of DatabaseAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DatabaseAdapter implements AdapterInterface
{
    private $source;
    
    private $manager;
    private static $mapManager = array();
    
    const MANAGER_EMPTY = 1;

    public static function registerManager($schema, $factory)
    {
        if (!is_array($schema)) {
            $schema = explode(',', $schema);
        }
        foreach ($schema as $sname) {
            self::$mapManager[trim($sname)] = $factory;
        }
        return $factory;
    }
    
    public function __construct($connectionString = '')
    {
        $connectionString && $this->connect($connectionString);
    }
    
    public function connect($connectionString) 
    {
        Assert::notEmpty($connectionString, 'Stringa connessione vuota');
        $this->manager = $this->makeManager(explode(':', $connectionString));
        $this->getManager()->connect();
        $this->getManager()->setParam('adapter', $this);
        return $this;
    }
    public function getManager()
    {
        Assert::notNull($this->manager, 'Manager non impostato', self::MANAGER_EMPTY);
        return $this->manager;
    }
    public function getName()
    {
        return $this->getManager()->getNameDB();
    }
    /**
     * 
     * @param type $info
     * @return class
     */
    private function makeManager($info)
    {
        $factory = Util\arrayGetAssert(self::$mapManager, $info[0], 'Schema non impostato : %s');
        $manager = is_string($factory) ? new $factory($this, $info) : call_user_func_array($factory, [$this, $info]);
        
        Assert::isInstanceOf($manager, __NAMESPACE__.'\\Database\\Manager');
        return $manager;
    }
    
    /**
     * L'adapter decide quale interfaccia debba implementare la struttura dati
     * @param type $struct
     * @return type
     */
    public function align($struct, $mainDS = null)
    {
        Assert::isInstanceOf($struct, Util\upName(__NAMESPACE__).'\\Core\\GetTableStructInterface');
        return $this->getManager()->align($struct->getTableStruct(), $mainDS);
    }

    public function delete($resource, $key)
    {
        return $this->getManager()->delete($resource, $key);
    }

    public function insert($resource, $data)
    {
        return $this->getManager()->insert($resource, $data);
    }

    public function find($resource, $fields, $pkey)
    {
        return $this->getManager()->find($resource, $fields, $pkey);
    }
    public function update($resource, $data, $key, $compare = false)
    {
        return $this->getManager()->update($resource, $data, $key, $compare);
    }
    
    public function load($query, $param = array(), $args = array())
    {
        return $this->getManager()->load($query, $param, $args);
    }

    public function first($query, $param = array(), $args = array())
    {
        return $this->getManager()->first($query, $param, $args);
    }
    
    public function getResourceInfo($name)
    {
        return $this->getManager()->getResourceInfo($name);
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    public function normalize($code)
    {
        if ($this->source) {
            return $this->source->normalize($code);
        }
    }
}
