<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database\PDO\Manager;

/**
 * Description of Sqlite manager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 * @codeCoverageIgnore
 */

use Spinit\Datasource\Adapter\Database\PDO\PDOManager;
use Webmozart\Assert\Assert;
use Spinit\Datasource\Adapter\Database\PDO\Serializer\SqliteSerializer;

use Spinit\Util;

class SqliteManager extends PDOManager
{
    public function __construct($database, $info) {
        parent::__construct($database, $info);
        $this->setSerializer(new SqliteSerializer());
    }
    protected function makePdo($info)
    {
        $pdo = null;
        array_shift($info);
        if (Util\arrayGet($info, 0)) {
            $filename = implode(':',$info);
            if (!is_file($filename) and Util\arrayGet($info,1)=='1') {
                $fp = fopen($filename, 'w');
                fclose($fp);
            }
            Assert::file($filename);
            $pdo = $this->newPdo('sqlite:'.$filename);
        } else if (Util\arrayGet($info, 1) == 'memory') {
            $pdo = $this->newPdo('sqlite::memory:');
        }
        if ($pdo) {
            $pdo->sqliteCreateFunction('md5', function ($str) {return md5($str); }, 1);
            return $pdo;
        }
        return null;
    }
    public function getResourceInfo($table)
    {
        $res = $this->first("SELECT name FROM sqlite_master WHERE type = 'table' and name = :name", array('name' => $table));
        if (!$res['name']) {
            return false;
        }
        $result = array();
        $data = $this->execQuery("PRAGMA table_info('{$table}')");
        foreach($data as $rec) {
            $type = Util\arrayGet($rec, 'type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $field = array(
                'pkey' => $rec['pk'],
                'type'=> $LVar[1][0],
                'size'=> $LVar[3][0],
                'notnull'=>$rec['notnull'],
                'default'=>$rec['dflt_value']
            );
            if ($field['type'] == 'varchar') {
                if ($field['default'] == 'NULL') {
                    $field['default'] = null;
                } else if ($field['default']{0} == "'") {
                    $field['default'] = substr(str_replace("''", "'", $field['default']), 1, -1);
                }
            }
            if ($field['type'] == 'datetime' and $field['default']) {
                $field['default'] = "({$field['default']})";
            }
            $result[$rec['name']] = $field;
        }
        return $result;
    }
    public function getDefaultDateTime()
    {
        return "(datetime('now','localtime'))";
    }
    /**
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function getNowDateTime()
    {
        return "datetime('now','localtime')";
    }
    
    public function alterColumn($table, $name, $conf)
    {
        return '';
    }
}
