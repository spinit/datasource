<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database\PDO\Manager;

/**
 * Description of MariadbManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

use Spinit\Datasource\Adapter\Database\PDO\PDOManager;
use Webmozart\Assert\Assert;
use Spinit\Util;

/**
 * @codeCoverageIgnore
 */
class MysqlManager extends PDOManager
{
    /**
     * Estrae il nome del DB dalle info
     * @return string
     */
    public function getNameDB() {
        return Util\arrayGet($this->getInfo(), 2);
    }
    
    /**
     * Formatta i parametri da passare alla costruttore PDO per la connessione al DB
     * @param type $info
     * @return type
     */
    protected function makePdo($info)
    {
        // estrazione
        $type = array_shift($info);
        $hostPort = explode('#', array_shift($info));
        $nameDB = array_shift($info);
        $username = array_shift($info);
        $password = array_shift($info);
        $create = array_shift($info);
        // Engine di default per la creazione delle tabelle (es: MEMORY)
        $engine = mb_strtoupper(array_shift($info));

        // impostazione
        $host = array_shift($hostPort);
        $port = Util\nvl(array_shift($hostPort), '3306');
        
        $opt = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4");
        $pdo = $this->newPDO("mysql:host={$host};port={$port}", $username, $password, $opt);
        $pdo->info = ['charset' => 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci', 'engine'=>$engine];
        try {
            $pdo->exec("USE {$nameDB}");
        } catch (\Exception $e) {
            if ($create) {
                $pdo->exec("CREATE DATABASE {$nameDB} ".$pdo->info['charset']);
            }
            $pdo->exec("USE {$nameDB}");
        }
        $pdo->schema = $nameDB;
        $pdo->type = $type;
        return $pdo;
    }
 
    public function getResourceInfo($name)
    {
        $res = $this->getTable($name);
        if (! Util\arrayGet($res, 'tbl')) {
            return false;
        }
        $result = array();
        foreach($this->execQuery("DESCRIBE {$res['scm']}.{$res['tbl']}") as $rec) {
            $type = Util\arrayGet($rec, 'Type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $field = array(
                'pkey' => $rec['Key'],
                'type'=> $LVar[1][0],
                'size'=> $LVar[3][0],
                'notnull'=>$rec['Null'] != 'YES',
                'default'=>$rec['Default']
            );
            $result[$rec['Field']] = $field;
        }
        return $result;
    }

    private function getTable($table)
    {
        $arTable = explode('.', $table);
        if (count($arTable)<2) {
            $schema = $this->getPdo()->schema;
            $table = $arTable[0];
        } else {
            $table = array_pop($arTable);
            $schema = implode('.', $arTable);
        }
        $sql = "SELECT table_schema as scm, table_name as tbl FROM information_schema.tables where table_name='{$table}' and table_schema = '{$schema}'";
        return $this->first($sql);
    }
    
    public function getDefaultDateTime()
    {
        return "CURRENT_TIMESTAMP";
    }
    
    public function getNowDateTime()
    {
        return "now()";
    }
    protected function createTableSql($table, $fields, $pkey)
    {
        $sql = parent::createTableSql($table, $fields, $pkey, '');
        $engine = $this->getPdo()->info['engine'];
        if ($this->getParam('status') == 'test') {
            $engine = 'MEMORY';
        }
        if ($engine) {
            $sql = str_replace('CREATE TABLE ', 'CREATE TEMPORARY TABLE ', $sql);
        }
        return $sql;
    }
}
