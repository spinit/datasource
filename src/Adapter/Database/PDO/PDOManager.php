<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database\PDO;

use Spinit\Datasource\Adapter\Database\Manager;
use Webmozart\Assert\Assert;
use Spinit\Datasource\Core\QueryInterface;
use Spinit\Datasource\Core\StructInterface;
use Spinit\Datasource\Adapter\Database\PDO\PDOSerializer;
use Spinit\Datasource\Adapter\Database\PDO\Helper\Command;
use Spinit\Datasource\TableStruct;

use Spinit\Util;
use Spinit\UUIDO;

use Spinit\Util\Error\NotFoundException;

/**
 * Description of ManagerPdo
 * Gestisce le chiamate ai database interfacciati tramite PDO
 * Offre una gestione unificata delle possibili operazioni da poter svolgere sui diversi DB lasciando
 * l'implementazioni delle caratteristiche peculiari ai propri figli.
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class PDOManager extends Manager
{
    private $pdo;
    private $serializer;

    private static $pdoList = array();

    private $manField = array(
        'traced'    => '__pref_id__',
        'tracing'   => '__pref_rec__',
        'suffix'    => '__',
        'deleting'  => '__is_del__',
        'opr_usr'   => '__usr_opr__',
        'opr_dat'   => '__dat_opr__'
    );

    private $tableStruct = '__datasource_struct__';
    /**
     *
     * @var Command
     */
    private $command = null;

    public function __construct($database, $info)
    {
        parent::__construct($database, $info);
        $this->setSerializer(new PDOSerializer());
        $this->pdoMaker = array($this, 'createPdo');
        $this->command = new Command($this);
    }
    public function reset()
    {
        self::$pdoList = array();
    }
    public function getCommand()
    {
        return $this->command;
    }
    /**
     * Funzione che serializza un dato nel campo della tabella in base al suo tipo
     * @param PDOSerializer $serializer
     */
    protected function setSerializer(PDOSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * Ogni figlio, in base alle informazioni presenti in info, decide quali parametri passare al pdo
     */
    abstract protected function makePdo($info);

    /**
     * Funzione standard per la generazione del PDO utilizzata dai suoi figli.
     *
     * @param type $dsn
     * @param type $username
     * @param type $password
     * @param type $options
     * @return \PDO
     */
    protected function newPdo($dsn, $username = null, $password = null, $options = null)
    {
        $pdo = new \PDO($dsn, $username, $password, $options);
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    /**
     * Verifica che la connessione non sia aperta. Altrimenti la apre
     */
    public function connect()
    {
        $name = implode(':', $this->getInfo());

        $this->pdo = Util\arrayGet(self::$pdoList, $name, function() use ($name) {
            $pdo = $this->makePdo($this->getInfo());
            Assert::notNull($pdo, 'Database non trovato : '.implode(':', $this->getInfo()));
            self::$pdoList[$name] = $pdo;
            $pdo->user = null;
            return $pdo;
        });
    }

    /**
     * Verifica di allineamento della struttura dati corrente con ciò che ci si aspetta
     * @param StructInterface $struct
     */
    public function align(StructInterface $struct)
    {
        $table = $struct->getName();
        // verifica esistenza della tabella di supporto per la descrizione della struttura dei dati
        $this->checkTableStruct($table);
        // se la tabella non è presente ... ne crea una nuova con un il primo campo e con i campi default
        if (!$this->getResourceInfo($table)) {
            $fields = array();
            $k = 0;
            foreach($struct->getFields() as $name => $field) {
                if (!$k or Util\arrayGet($field, 'default') or Util\arrayGet($field, 'pkey')) {
                    $fields[$name] = $field;
                }
                $k++;
            }
            $this->createTable($table, $fields, !in_array($struct->getParam('trace'), ['0', 'no']));
        }
        // aggiunge i rimanenti
        $this->updateTable($struct);
    }

    /**
     * Verifica se esiste la tabella di configurazione dei dati. Siccome tale verifica viene effettuata alla richiesta
     * di verifica di una qualsiasi tabella allora, per evitare loop, l'aggiornamento viene fatto solo se la tabella
     * richiedente è diversa da quella di configurazione.
     *
     * @param type $table
     * @return type
     */
    private function checkTableStruct($table)
    {
        if ($table == $this->tableStruct) {
            // stop loop di richiesta verifiche
            return;
        }
        if (!$this->getResourceInfo($this->tableStruct)) {
            $this->align(new TableStruct(array(
                'name' => $this->tableStruct,
                'param' => array('timing'=>'no', 'trace'=>'no'),
                'fields' => array(
                    'id'    => array('type'=>'binkey', 'pkey'=>'1'),
                    'ctx'   => array('type' => 'varchar'),
                    'typ'   => array('type' => 'varchar'),
                    'nme'   => array('type' => 'varchar'),
                    'par'   => array('type' => 'varchar'),
                    'cnf'   => array('type' => 'varchar'),
                )
            )));
        }
    }

    /**
     * Inserisce nella struttra di configurazione se informazioni sui campi
     * @param type $table
     * @param type $type
     * @param type $name
     * @param type $cnf
     * @return type
     */
    public function addResourceStruct($table, $type, $name, $cnf = '')
    {
        $data = array(
            'ctx'=>'table.field',
            'typ'=>$type,
            'nme'=>$name,
            'par'=>$table,
            'cnf'=>$cnf,
        );
        $pkey = $this->insert($this->tableStruct, $data);
        return $pkey;
    }
    /**
     * Restituisce la lista dei campi memorizzati struttura di supporto
     * @param type $table
     * @param type $type
     * @return type
     */
    public function getResourceStruct($table, $type = '')
    {
        $sql = "
                select /*id,*/ nme as name, typ as type, cnf as conf
                from {$this->tableStruct}
                where ctx = 'table.field'
                  and par = '{{table}}'
                  ";
        // se non è stato richiesto un particolare tipo ... allora vengono restituiti tutti
        if (!$type) {
            $ds = $this->load($sql, array('table'=>$table));
        } else {
            // altrimenti viene filtrato sul tipo richiesto
            $sql .=  "and typ = '{{type}}'";
            $ds = $this->load($sql, array('table'=>$table, 'type' => $type));
        }
        $result = array();
        foreach($ds as $rec) {
            //$rec['id'] = strtoupper(bin2hex($rec['id']));
            $result[$rec['name']] = $rec;
        }
        return $result;
    }
    /**
     * Genera la tabella e l'eventuale trace
     * @param type $table
     * @param type $fields
     * @param type $trace
     * @return boolean
     * @throws \Exception
     */
    private function createTable($table, $fields, $trace)
    {
        $list = array();
        $pkey = array();
        foreach($fields as $name => $conf) {
            if (Util\arrayGet($conf, 'pkey')) {
                $pkey[]= $name;
            }
            $list []= $this->createField($table, $name, $conf);
        }
        $sql = 'SQL non inizializzato';
        try {
            $sql = $this->createTableSql($table, $list, $pkey);
            $this->execCommand($sql);
            if ($trace) {
                $tableTrace = $table.$this->manField['suffix'];
                $this->execCommand("DROP TABLE IF EXISTS ".$tableTrace);
                $sql = $this->createTableSql($tableTrace, $list, $pkey);
                $this->execCommand($sql);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage()."\n\n".$sql);
        }
        return true;
    }

    protected function createTableSql($table, $fields, $pkey)
    {
        if (count($pkey)) {
            $fields[] = 'PRIMARY KEY ('.implode(', ', $pkey).')';
        }
        return "CREATE TABLE {$table}(\n".implode(",\n", $fields).')';
    }

    /**
     * @codeCoverageIgnore
     * @param type $conf
     * @return type
     */
    public function getType($conf)
    {
        switch(@$conf['type']) {
            case 'json':
                $type = 'text';
                break;
            case 'binkey':
                $size = Util\arrayGet($conf, 'size', '16');
                $type = "binary({$size})";
                break;
            default:
                $type = $conf['type'];
                $size = Util\arrayGet($conf, 'size', $type == 'varchar' ? '50':'');
                if ($size) {
                    $type.= "({$size})";
                }
                break;
        }
        return $type;
    }

    public function quote($content, $wrap = false)
    {
        if ($content === null) {
            return $wrap ? 'NULL' : '';
        }
        if (!$wrap) {
            return substr($this->pdo->quote($content), 1, -1);
        }
        return $this->pdo->quote($content);
    }
    /**
     * Nella determinazione del valore di default durante la creazione si tiene conto del tipo
     * @param type $conf
     * @return string
     */
    public function getDefault($conf)
    {
        if (!isset($conf['default'])) {
            if (isset($conf['pkey'])) {
                return '';
            }
            return '';
            //return ' DEFAULT NULL';
        }
        $str = ' DEFAULT ';
        switch(Util\arrayGet($conf, 'type')) {
            case 'binkey':
                return '';
            case 'varchar':
            case 'char':
            case 'json':
                $conf['default'] = $this->quote($conf['default'], true);
                break;
            case 'date':
            case 'datetime':
            case 'timestamp':
                if ($conf['default'] == 'now') {
                    $conf['default'] = $this->getDefaultDateTime();
                }
        }
        $str .= $conf['default'];

        return $str;
    }

    /**
     * Snippet per la creazione del campo della tabella
     * @param type $table
     * @param type $name
     * @param type $conf
     * @return type
     */
    public function createField($table, $name, $conf)
    {
        $this->fields[$table][$name] = $conf;
        $str  = $name. ' '. $this->getType($conf);
        $str .= $this->getDefault($conf);
        return $str;
    }

    /**
     * Aggiunge i campi non presenti nella tabella (+ eventualmente quella di trace)
     * @param type $table
     * @param type $expected
     * @param type $actual
     */
    private function updateTable($struct)
    {
        $table = $struct->getName();
        $trace = !\in_array($struct->getParam('trace'),['0', 'no']);
        if ($trace) {
            $tableTrace = $table.$this->manField['suffix'];
            if (!$this->getResourceInfo($tableTrace)) {
                $actual = $this->getResourceInfo($table);
                $this->createTable($tableTrace, $actual, false);
            }
        }
        foreach([$struct->getFields(), $this->getDefaultFields($struct)] as $listField) {
            $actual = $this->getResourceInfo($table);
            $this->updateTableField($table, $listField, $actual, $trace);
        }
    }

    private function updateTableField($table, $listField, &$actual, $trace)
    {
        foreach($listField as $name => $conf) {
            if (!isset($actual[$name])) {
                foreach( $this->addField($table, $name, $conf, $trace, $actual) as $sql) {
                    if (!$sql) {
                        continue;
                    }
                    $this->execCommand($sql);
                }
                $actual[$name] = $conf;
            }
        }
    }

    private function getDefaultFields($struct)
    {
        // lista dei campi da aggiungere
        $list = new \ArrayObject();

        // chiave della tabella (non multipla)
        $key = null;
        foreach($struct->getFields() as $conf) {
            if (Util\arrayGet($conf, 'pkey')) {
                $key = $conf;
                break;
            }
        }

        // campi relativi alla modifica
        if (!in_array($struct->getParam('timing'),['0','no'])) {
            foreach(['ins', 'upd', 'del'] as $when) {
                foreach(['dat' => 'datetime', 'usr'=>'binkey'] as $who => $type) {
                    $list["{$who}_{$when}"] = array('type' => $type);
                }
            }
        }

        // campi relativi al tracciamento
        if (!in_array($struct->getParam('trace'), ['0','no']) and $key) {
            $list[$this->manField['traced']] = array('type'=>Util\arrayGet($key, 'type', 'binkey'));
        }

        return $list;
    }

    /**
     * Crea lo snippet per aggiungere il campo alla tabella
     * Effettua anche le aggiunte necessarie alla tabella di trace
     * @param type $table
     * @param type $name
     * @param type $conf
     * @return string
     */
    private function addField($table, $name, $conf, $trace, &$actual)
    {
        try {
            $cmds = $this->makeFieldType(Util\arrayGet($conf, 'type'))->addField($table, $name, $conf, $trace);
        } catch (NotFoundException $ex) {
            $cmds = [$this->addColumn($table, $name, $conf)];
            if ($trace) {
                $table .= $this->manField['suffix'];
                $cmds[] = $this->addColumn($table, $name, $conf);
                if ($name == $this->manField['traced']) {
                    $key = '';
                    foreach($actual as $name => $act) {
                        if ($act['pkey']) {
                            $key = $name;
                            break;
                        }
                    }
                    Assert::notEmpty($key, 'Chiave mancante');
                    $cmds[] = $this->alterColumn($table, $key, ['type'=>'binkey']);
                    $cmds[] = $this->addColumn($table, $this->manField['tracing'], $actual[$key]);
                    $cmds[] = $this->addColumn($table, $this->manField['deleting'], ['type'=>'integer', 'size'=>'1']);
                    $cmds[] = $this->addColumn($table, $this->manField['opr_usr'], ['type'=>'binkey']);
                    $cmds[] = $this->addColumn($table, $this->manField['opr_dat'], ['type'=>'datetime']);
                }
            }
        }
        return $cmds;
    }
    protected function alterColumn($table, $name, $conf)
    {
        return "ALTER TABLE {$table} MODIFY COLUMN ".$this->createField($table, $name, $conf);
    }
    protected function addColumn($table, $name, $conf)
    {
        return "ALTER TABLE {$table} ADD COLUMN ".$this->createField($table, $name, $conf);
    }
    public function makeFieldType($type)
    {
        $class = __NAMESPACE__. '\\Type\\' .ucfirst(strtolower($type)).'Type';
        if (class_exists($class)) {
            return new $class($this);
        }
        throw new NotFoundException();
    }
    /**
     * Esegue la query determinando lo script da eseguire
     * @param type $query
     * @param type $params
     * @return type
     */
    public function load ($query, $params = array(), $args = array())
    {
        return $this->execQuery($this->getSql($query), $params, $args, $query);
    }

    /**
     * Estrazione del comando sql da eseguire presente nella query
     * @param type $query
     * @return type
     */
    protected function getSql($query)
    {
        if (is_string($query)) {
            return $query;
        }
        // query specifica per il tipo database utilizzato
        $type = Util\arrayGet($this->getInfo(), 0);
        $sql  = Util\arrayGet($query, $type);
        if (!$sql) {
            // query generica
            $sql  = Util\arrayGet($query, 'sql');
        }
        Assert::notEmpty($sql, "Query not found");
        return $sql;
    }

    private function extractValues($params)
    {
        $args = [];
        foreach($params as $name => $value) {
            if (is_array($value)) {
                $args[$name] = Util\arrayGet($value, 'value');
            } else {
                $args[$name] = $value;
            }
        }
        return $args;
    }
    /**
     * Esecuzione dello script normalizzandolo rispetto ai parametri passati
     * @param type $sql
     * @param type $params
     * @return type
     */
    public function execCommand($sql, $params = array())
    {
        try {
            list($sql, $params) = $this->normalizeExec($sql, $params);
            if (count($params)) {
                $rs = $this->pdo->prepare($sql);
                return $rs->execute($params);
            }
            return $this->pdo->exec($sql);
        } catch (\Exception $e) {
            throw new \Exception ($sql.' '.json_encode($params).' '.$e->getMessage());
        }
    }

    /**
     * Esecuzione della query normalizzandola rispetto ai parametri passati
     * @param type $sql
     * @param type $params
     * @return \Spinit\Datasource\Adapter\Database\PDO\PDODataSet
     */
    public function execQuery($sql, $params = array(), $args = array(), $query = null)
    {
        $oparams = $params;
        list($sql, $params) = $this->normalizeExec($sql, $params, $args);
        $rs = false;
        try {
            if (count($params)) {
                $rs = $this->pdo->prepare($sql);
                $rs->execute($params);
            } else {
                $rs = $this->pdo->query($sql);
            }
        } catch(\Exception $e) {
            throw new \Exception(json_encode(array($e->getMessage(), $sql, $oparams), JSON_PRETTY_PRINT));
        }
        Assert::notSame($rs, false);
        return new PDODataSet($rs);
    }

    /**
     * La normalizzazione della query avviene in due passi
     * - viene chiesto al database se occorre modificare lo script individuato (es. effettuare delle sostituzioni
     *   con dei placeolder)
     * - vengono sostituiti nella query stessa i parametri che sono presenti nella forma {{@?parametro}}
     * @param type $sqlraw
     * @param type $params
     * @return type
     */
    private function normalizeExec($sqlraw, $params, $args = array())
    {
        $params = $this->extractValues($params);
        $args = $args ? $args : [];
        // sostituzione nome database
        $sql = $this->getDataBase()->normalize($sqlraw);
        if (!$sql or !is_string($sql)) {
            $sql = $sqlraw;
        }
        $args;
        $sql = $this->checkNormalizeValue('/\{\{([^ ]+)\}\}/', $params, $sql);
        $sql = $this->checkNormalizeValue('/\{\[([^ ]+)\]\}/', $args, $sql, 1);

        $sql = str_replace(array("!= NULL", "<> NULL"), array(' IS NOT NULL', ' IS NOT NULL'), $sql);
        $sql = str_replace(array("= NULL"), array(' IS NULL'), $sql);
        $this->sql = (count($params) or count($args))? array('sql'=>$sql, 'par'=>$params, 'arg' => $args) : $sql;

        if (getenv('SPINIT_DATASOURCE_DEBUG')=='QUERY') {
            Util\console("Query SQL", $this->sql);
        }
        // viene restituita la query pronta per l'esecuzione con l'insieme di parametri che sono affidati al driver del DB
        return array($sql, $params);
    }

    private function checkNormalizeValue($pattern, &$data, $sql)
    {
        preg_match_all($pattern, $sql, $matches);
        foreach ($matches[1] as $k => $name) {
            $sql = $this->normalizeValue($name, $data, $matches[0], $k, $sql);
        }
        return $sql;
    }

    private function normalizeValue($name, &$params, $matches, $k, $sql)
    {
        $field = in_array($name{0}, array('@')) ? substr($name, 1) : $name;
        if (array_key_exists($field, $params)) {
            switch($name{0}) {
                case '@':
                    $value = $this->serializeValue($params[$field], 'binary');
                    break;
                default :
                    $value = $this->quote($params[$field]);
                    break;
            }
            unset($params[$field]);
        } else {
            switch($name{0}) {
                case '@':
                    $value = null;
                    break;
                default :
                    $value = $this->quote('');
                    break;
            }
        }
        $sql = str_replace($matches[$k], $value  === null ? 'NULL' : $value, $sql);
        return $sql;
    }
    /**
     * Restituisce solo il primo record
     * @param type $query
     * @param type $params
     * @return type
     */
    public function first($query, $params = array(), $args = array())
    {
        $dataset = $this->execQuery($this->getSql($query), $params, $args);
        $current = $dataset->current();
        $dataset->close();
        return $current;
    }

    /**
     * Inserisce l'array passato nella risorsa indicata
     * @param type $resource
     * @param type $dataList
     * @return type
     */
    public function insert($resource, $data)
    {
        return $this->getCommand()->insert($resource, $data);
    }

    public function update($resource, $data, $key, $compare = false)
    {
        if ($this->isSame($data, $compare)) {
            return;
        }
        $this->trace($resource, $key, false);
        return $this->getCommand()->update($resource, $data, $key, '', $compare);
    }

    private function isSame($data, $compare)
    {
        if (!$compare) {
            return false;
        }
        foreach($data as $k => $v) {
            if (!isset($compare[$k]) or $compare[$k] != $v) {
                return false;
            }
        }
        return true;
    }
    public function delete($resource, $key)
    {
        $this->trace($resource, $key, true);
        return $this->getCommand()->delete($resource, $key);
    }

    public function find($resource, $fieldList, $pkey)
    {
        return $this->getCommand()->find($resource, $fieldList, $pkey);
    }

    public function getNextValue($type, $size)
    {
        switch($type) {

            case 'binary':

                switch($size) {
                    case 16 :
                        return strtoupper((string) $this->getParam('adapter')->getSource()->getCounter()->next());
                }
                return strtoupper((string) UUIDO\randHex(Util\nvl($size, 8) * 2));

            case 'smallint':
            case 'bigint':
            case 'int':
            case 'integer':

                return rand(1, 9999);
        }
        return '';
    }
    /**
     * Restituisce un array che contiene il nome di riferimento ed il valore da associargli.
     * Così facendo, se serialize lancia una eccezione ExceptionValue, il command... potrà mettere
     * il valore direttamente nella query invece che passarlo tra i parametri
     *
     * @param type $type
     * @param type $name
     * @param type $value
     * @return array
     */
    public function getRefNameAndValue($type, $name, $value)
    {
        if (is_array($value)) {
            return ["{$value[1]}(:$name)", $this->getSerializer()->serialize($value, $type, $value[0])];
        }
        return [':'.$name, $this->getSerializer()->serialize($value, $type, $value)];
    }

    public function formatColumn($type, $name)
    {
        switch($type) {
            case 'binary':
            case 'binkey':
                return "HEX({$name})";
        }
        return $name;
    }
    public function getPdo()
    {
        return $this->pdo;
    }
    public function serializeValue($value, $type, $default = '')
    {
        try {
            if ($value === null) {
                return 'NULL';
            }
            return $this->getSerializer()->serialize($value, $type, $default);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function unserializeValue($value, $type)
    {
        return $this->getSerializer()->unserialize($value, $type);
    }
    private function trace($resource, $pkey, $isDeleted)
    {
        $info = $this->getResourceInfo($resource.$this->manField['suffix']);
        if ($info) {
            // salvataggio record e relative impostazioni
            $commands = $this->getCommand()->trace(
                $resource,
                $pkey,
                $this->manField,
                $isDeleted);
        }
    }
}
