<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datasource\Adapter\Database\PDO;

use Spinit\Util;
use Spinit\Datasource\Adapter\Database\PDO\Helper\ExceptionValue;
/**
 * Description of PDOSerializer
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDOSerializer
{
    public function serialize($value, $type, $default = '')
    {
        if (is_array($value)) {
            $value = Util\arrayGet($value, 'value');
        }
        switch($type) {
            case 'date':
            case 'datetime':
            case 'timestamp':
                return $this->serializeDate($value, $default);
            case 'binary':
            case 'binkey':
                return $this->serializeBinkey($value, $default);
            case 'int':
            case 'integer':
            case 'bigint':
                if ($value === '') {
                    return null;
                }
                break;
        }
        return Util\nvl($value, $default);
    }
    public function unserialize($value, $type)
    {
        return $value;
    }
    protected function serializeDate($value, $default)
    {
        switch($value) {
            case '':
                return null;
            case 'now':
                return date('Y-m-d H:i:s');
        }
        
        $part = explode(' ', $value);
        $struct = explode('/', $part[0]);
        if (count($struct)>1) {
            $struct = array_reverse($struct);
            $part[0] = implode('-', $struct);
        } 
        return implode(' ', $part);
    }
    protected function serializeBinkey($value, $default)
    {
        if (!$value) {
            return null;
        }
        throw new ExceptionValue("UNHEX('{$value}')");
    }
}
