<?php
namespace Spinit\Datasource\Adapter\Database\PDO\Type;
/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

use Spinit\Datasource\TableStruct;
use Spinit\Util;
use Spinit\Datasource\Adapter\Database\PDO\Type;

/**
 * Description of ListType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ListType extends Type
{
    public function addField($table, $name, $conf, $trace)
    {
       // lista dei campi LIST conosciuti sulla tabella.
        $confList = $this->getManager()->getResourceStruct($table, 'list');
        
        // se il campo in analisi non è presente .. 
        if (!isset($confList[$name])) {
            // occorre verificare se la tabella base è configurata a dovere per ospitare oggetti LIST
            $this->checkTableList($table);
            // viene configurato il campo "name" a gestire elementi LIST
            $this->getManager()->addResourceStruct($table, 'list', $name, Util\arrayGet($conf,'conf'));
        }
        return array();
    }
    private function checkTableList($table)
    {
        $this->getManager()->align(new TableStruct(array(
                'name' => $table,
                'fields' => array(
                    '__list__' => array('type'=>'json')
                )
            )
        ));
        // nome della tabella dove mantenere i dati di list
        $tableList = $table.'__L';
        // creazione tabella
        if (!$this->getManager()->getResourceInfo($tableList)) {
            $this->getManager()->align(new TableStruct(array(
                'name' => $tableList,
                'param'=> ['trace' =>'no'],
                'fields' => array(
                    'id'    => array('type'=>'binkey', 'pkey'=>'1'),
                    'id_rel'   => array('type' => 'binkey'),
                    'id_ref'    => array('type'=>'binkey'),
                    'nme'    => array('type'=>'varchar'),
                    'tbl_rel'    => array('type'=>'varchar'),
                    'tbl_ref'    => array('type'=>'varchar'),
                    'act'    => array('type'=>'int', 'default'=>'1'),
                    'val_ref'    => array('type'=>'binkey'),
                    'val_str'    => array('type'=>'varchar'),
                    'val_int'    => array('type'=>'bigint'),
                )
            )));
        }
        return $tableList;
    }
    
    public function getData($table, $pkey, $param = array())
    {
        
        $listTable = $this->checkTableList($table);
        
        $data = array();
        $vkey =  array_shift($pkey);
        foreach($param as $name => $conf) {
            $field = $this->getFieldValName(Util\arrayGet($conf, 'conf'));
            if ($field == 'val_ref') {
                $field = 'HEX(val_ref)';
            }
            foreach($this->getManager()
                         ->load("select HEX(id) as id, HEX(id_ref) as ref, {$field} as val   from {$listTable} where id_rel = {{@key}} and nme = '{{nme}}'",
                                 array('key' => $vkey, 'nme' => $name)) as $row) {
                // $data[$name][] = array($row['ref'] ?: null, $row['val'], $row['id']);
                $data[$name][] = $row['ref'];
            }
        }
        return $data;
    }
    
    private function getFieldValName($conf)
    {
        $field = '';
        switch($conf) {
            case 'str':
                $field = 'val_str';
                break;
            case 'int':
                $field = 'val_int';
                break;
            default:
                $field = 'val_ref';
                break;
        }
        return $field;
    }
    public function save($conf, $table, $data, $pkey, $compare = false)
    {
        //var_dump(func_get_args());exit;
        $listTable = $this->checkTableList($table);
        $vkey = (string) array_shift($pkey);
        if (isset($data[$conf['name']])) {
            // i dati devono essere aggiornati?
            $values = $data[$conf['name']] ;
            // occorre fare veramente l'aggiornamento?
            if ($compare and isset($compare[$conf['name']])) {
                if (json_encode($values) == json_encode($compare[$conf['name']])) {
                    // i due insieme sono uguali
                    return;
                }
            }
            // i record da modificare vengono disattivati
            $this->getManager()->update($listTable, ['act'=>'0'], ['id_rel'=>$vkey, 'nme'=>$conf['name']]);
            // si cicla sugli elementi inviati
            foreach($values ?: array() as $item) {
                if (is_array($item)) {
                    $list = ['ref' => array_shift($item), 'val' => array_shift($item), 'id' => array_shift($item)];
                } else {
                    switch($conf['conf']) {
                        case 'str':
                            $list= ['ref' => null, 'val' => (string) $item];
                            break;
                        default:
                            $list = ['ref' => (string)$item, 'val' => null];
                            break;
                    }
                }
                $rec = ['id_rel' => $vkey, 'nme'=>$conf['name'], 'id_ref'=> $list['ref'], 'act'=>'1'];
                $field = $this->getFieldValName(Util\arrayGet($conf, 'conf'));
                $rec[$field] = $list['val'];
                if (Util\arrayGet($list, 'id')) {
                    $this->getManager()->update($listTable, $rec, $list['id']);
                } else {
                    $id = $this->getManager()->insert($listTable, $rec);
                }
            }
            // i record ancora disattivati vengono cancellati
            $this->getManager()->delete($listTable,['id_rel'=>$vkey, 'nme'=>$conf['name'], 'act'=>'0']);
            $this->saveDataList($table, $vkey, $listTable);
        }
    }
    
    /**
     * Salva in $table.__list__ la configurazione presente in $listTable in formato json
     * @param type $table
     * @param type $listTable
     */
    private function saveDataList($table, $vkey, $listTable)
    {
        $conf = $this->getManager()->getResourceStruct($table, 'list');
        $sql = "select nme, hex(id_ref) id_ref, val_str, hex(val_ref) val_ref, val_int  from {$listTable} where id_rel = ".$this->getManager()->serializeValue($vkey, 'binkey');
        $list = [];
        foreach($this->getManager()->execQuery($sql) as $record) {
            switch(Util\arrayGet($conf[$record['nme']], 'conf')) {
                case '':
                    $list[$record['nme']][] = $record['id_ref'];
                    break;
                case 'str':
                    $list[$record['nme']][] = $record['val_str'];
                    break;
            }
        }
        $this->getManager()->execCommand(
            "update {$table} set __list__ = :list where id = ".$this->getManager()->serializeValue($vkey, 'binkey'),
            ['list'=>json_encode($list)]
        );
    }
}
