<?php

/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database\PDO\Helper;

use Spinit\Util;
use Spinit\Datasource\Adapter\Database\PDO\Helper\ExceptionValue;
use Spinit\Datasource\Adapter\Database\PDO\PDOManager;
use Spinit\Datasource\Adapter\Database\PDO\Type\ListType;

/**
 * Description of Command
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Command
{
    private $pdom;
    
    public function __construct($pdoManager)
    {
        $this->pdom = $pdoManager;
    }
    
    /**
     * 
     * @return PDOManager
     */
    public function getManager()
    {
        return $this->pdom;
    }
    
    /**
     * Determina quali valori sono da passare tramite parametri e quali invece vanno indicati direttamente nella query
     * @param type $info
     * @param type $list
     * @param type $data
     * @param boolean $assemble definisce se il valore debba essere assemblato con il nome o debba essere restiuito da solo
     */
    protected function makeDataRefList($info, $list, $data, $suffix = '', $assemble = true)
    {
        $result = [];
        foreach($list as $name => $value) {
            // L'array $data viene impostato con il valore che verrà associato al riferimento
            // Se c'è una diversa "disposizione" essa sarà presente nell'eccezione
            try {
                $param = $name.$suffix;
                if (!isset($info[$name])) {
                    continue;
                }
                list($ref, $data[$param]) = $this->getManager()->getRefNameAndValue($info[$name]['type'], $param, $value);
            } catch (ExceptionValue $ex) {
                $ref = $ex->getMessage();
            }
            $result[] = $assemble ? $name.' = '.$ref : $ref;
        }
        return $result;
    }
    
    /**
     * Determina quali valori sono da passare tramite parametri e quali invece vanno indicati direttamente nella query
     * @param type $info
     * @param type $list
     * @param type $data
     */
    protected function makeDataRefAssoc($info, $list, $data, $when)
    {
        $result = [];
        foreach($list as $name => $value) {
            if (!isset($info[$name])) {
                continue;
            }
            try {
                list($result[$name], $data[$name]) = $this->getManager()->getRefNameAndValue($info[$name]['type'], $name, $value);
            } catch (ExceptionValue $ex) {
                $result[$name] = $ex->getMessage();
            }
        }
        return $result;
    }
    
    private function getPkeyInfo($info, &$dataList)
    {
        $pkey = [];
        $vkey = null;
        foreach($info as $name => $conf) {
            if (Util\arrayGet($conf, 'pkey')) {
                $pkey[] = $name;
                if (isset($dataList[$name])) {
                    $vkey = [$name => $dataList[$name]];
                }
            }
        }
        // se la primary key è singola ed è vuota => si preoccupa di inizializzarla
        if (count($pkey)== 1 and $vkey === null) {
            $vkey = [$pkey[0] => $this->getManager()->getNextValue(
                Util\arrayGet($info[$pkey[0]], 'type'),
                Util\arrayGet($info[$pkey[0]], 'size')
            )];
            $dataList[$pkey[0]] = $vkey[$pkey[0]];
        }
        return $vkey;
    }
    
    public function insert($tableName, $dataList)
    {
        $data = new \ArrayObject();
        // struttura tabella
        $info = $this->getManager()->getResourceInfo($tableName);
        // verifica e generazione primary key con eventuale aggiornamento dataList
        $vkey = $this->getPkeyInfo($info, $dataList);
        // INSERIMENTO RECORD PRINCIPALE ..... genera :
        // - l'insieme delle colonne
        // - l'insieme dei parametri e dei valori per values
        // - l'insieme dei dati da passare ai parametri
        $list = $this->makeDataRefAssoc($info, $dataList, $data, 'ins');
        //verifica data inserimento + utente inserimento
        if (isset($info['dat_ins']) and !isset($list['dat_ins'])) {
            $list['dat_ins'] = $this->getManager()->quote($this->getManager()->serializeValue('now', $info['dat_ins']['type']), true);
        }
        if (isset($info['usr_ins']) and !isset($list['usr_ins'])) {
            try {
                $list['usr_ins'] = $this->getManager()->serializeValue(Util\getenv('USER_ID'), $info['usr_ins']['type']);
                if ($list['usr_ins'] === null) {
                    $list['usr_ins'] = 'NULL';
                }
            } catch (\Exception $ex) {
                $list['usr_ins'] = $ex->getMessage();
            }
        }
        $columns = implode(', ', array_keys($list));
        $values = implode(', ', array_values($list));
        $cmd = "INSERT INTO {$tableName} ({$columns}) VALUES ({$values})";
        $this->getManager()->execCommand($cmd, $data);
        // INSERIMENTO VALORI STRUTTURATI
        $this->saveStructData($tableName, $dataList, $vkey);
        return $vkey;
    }
    
    public function update($tableName, $dataList, $pkey, $suffix = '', $compare = false)
    {
        $debug = Util\getenv('DEBUG') ? : function($msg){};
        
        $data = new \ArrayObject();
        $info = $this->getManager()->getResourceInfo($tableName);
        $vkey = $this->normalizePkey($pkey);
        //verifica data inserimento + utente inserimento
        if (isset($info['dat_upd']) and !$suffix) {
            $dataList['dat_upd'] = 'now';
        }
        if (isset($info['usr_upd']) and !$suffix) {
            $dataList['usr_upd'] = Util\getenv('USER_ID');
        }
        $sets = implode(', ', $this->makeDataRefList($info, $dataList, $data, '__set__'.$suffix));
        $cnds = implode(' AND ', $this->makeDataRefList($info, $vkey, $data, '__cnd__'.$suffix));
        $command = [
            'sql' =>"UPDATE {$tableName} SET {$sets} WHERE {$cnds}", 
            'param' =>(array) $data
        ];
        $debug($command);
        $ret = $this->getManager()->execCommand($command['sql'], $command['param']);
        if ($ret) {
            $this->saveStructData($tableName, $dataList, $vkey, $compare);
        }
        return $ret;
    }
    
    private function saveStructData($tableName, $dataList, $pkey, $compare = false)
    {
        $struct = $this->getManager()->getResourceStruct($tableName);
        foreach($struct as $conf) {
            // al tipo è richiesto di salvare i dati in formato corretto nella tabella
            $type = $this->getManager()->makeFieldType($conf['type']);
            $type->save($conf, $tableName, $dataList, $pkey, $compare);
        }
    }
    
    public function delete($tableName, $pkey)
    {
        $data = new \ArrayObject();
        $info = $this->getManager()->getResourceInfo($tableName);
        // genera :
        // - l'insieme delle colonne
        // - l'insieme dei parametri e dei valori per values
        // - l'insieme dei dati da passare ai parametri
        $cnds = implode(' AND ', $this->makeDataRefList($info, $this->normalizePkey($pkey), $data));
        $command = [
            'sql' =>"DELETE FROM {$tableName} WHERE {$cnds}", 
            'param' =>(array) $data
        ];
        $ret = $this->getManager()->execCommand($command['sql'], $command['param']);
        return $ret;
    }
    
    /**
     * Prepara le query da eseguire per estrapolare i dati  richiesti
     * @param type $tableName
     * @param type $fieldList
     * @param type $pkey
     * @return type
     */
    public function find($tableName, $fieldList, $pkey)
    {
        $debug = Util\getenv('DEBUG') ? : function($ms){};
        $pkey  = $this->normalizePkey($pkey);
        $info = $this->getManager()->getResourceInfo($tableName);
        $struct = $this->getManager()->getResourceStruct($tableName, 'list');
        $select = [];
        $list = [];
        $param = new \ArrayObject();
        $data = new \ArrayObject();
        foreach(Util\asArray($fieldList, ',') as $name => $field) {
            if (is_array($field)) {
                $field = Util\arrayGet($field, 'name', $name);
            }
            if ($field == '*') {
                $select[] = $field;
            } else if (isset($info[$field])) {
                $select [] = $this->getInfoField($info, $field);
            } else if (isset($struct[$field])){
                $list[$field] = $struct[$field];
            }
        }
        $cnds = implode(' AND ', $this->makeDataRefList($info, $pkey, $param));
        $fields = implode(', ', $select);
        $data =  $this->getManager()->first("SELECT {$fields} FROM {$tableName} WHERE {$cnds}", $param);
        
        $debug([$this->getManager()->sql, $data]);
        // se ha dati di tipo list ... allora occorre completare il record che contiene il risultato
        if (count($struct)) {
            $data = $this->completeListData($data, $tableName, $pkey, $list);
        }
        return $data;
    }
    
    /**
     * Determina come formattare il campo nella dichiarazione SELECT
     * @param type $info
     * @param string $field
     * @return string
     */
    private function getInfoField($info, $field)
    {
        try {
            $format = $this->getManager()->formatColumn(Util\arrayGet($info[$field], 'type'), $field);
        } catch (ExceptionValue $ex) {
            $format = $ex->getMessage();
        }
        if ($format != $field) {
            $format .= ' as ' . $field;
        }
        return $format;
    }
    
    /**
     * Completa i dati con i campi LIST richiesti
     * @param type $table
     * @param type $pkey
     * @param type $list
     * @param type $conf
     */
    private function completeListData($data, $table, $pkey, $list)
    {
        if (substr($table, -2) == '__') {
            return $data;
        }
        $listType = $this->getManager()->makeFieldType('list');
        // inizializzazione
        foreach($list as $nme => $conf) {
            $data[$nme] = null;
        }
        foreach($listType->getData($table, $pkey, $list) as $field => $result) {
            $data[$field] = $result;
        }
        return $data;
    }
    private function normalizePkey($pkey)
    {
        if (!is_array($pkey)) {
            return ['id' => $pkey];
        }
        return $pkey;
    }
    
    public function trace($fromResource, $pkey, $conf, $isDeleted)
    {
        $data = new \ArrayObject();
        $fromKey = $this->normalizePkey($pkey);
        $keyList = array_keys($fromKey);
        $keyName = array_shift($keyList);
        $toResource = $fromResource.$conf['suffix'];
        
        $info = $this->getManager()->getResourceInfo($fromResource);
        $infoTo = $this->getManager()->getResourceInfo($toResource);
        
        $values = [$conf['opr_usr'] => Util\getenv('USER_ID'), $conf['opr_dat'] => 'now'];
        $values = implode(', ', $this->makeDataRefList($infoTo, $values, $data, 'trace', false));
        $cnds = implode(' AND ', $this->makeDataRefList($info, $fromKey, $data));
        // nuova chiave
        $vkey = $dataList[$keyName] = $this->getManager()->getNextValue(
            Util\arrayGet($infoTo[$keyName], 'type'),
            Util\arrayGet($infoTo[$keyName], 'size')
        );
        // copia dati nella tabella di trace
        $fields = [];
        foreach(array_keys($info) as $field) {
            if ($field == $keyName) {
                try {
                    $this->getManager()->getSerializer()->serialize($vkey, 'binkey');
                } catch (\Exception $e) {
                    $fields[$field] = $e->getMessage();
                }
            } else {
                $fields[$field] = $field;
            }
        }
        $selectFields = implode(', ', $fields);
        $insertFields = implode(', ', array_keys($fields));
        $command = ['sql' =>"
            insert into {$toResource} ({$insertFields}, {$conf['tracing']}, {$conf['deleting']}, {$conf['opr_usr']}, {$conf['opr_dat']})
            select {$selectFields}, {$keyName}, ".($isDeleted?'1':'0').", {$values}
            from {$fromResource}
            where ".$cnds, 
            'param'=>(array) $data];
        //var_dump($vkey, $fromKey);exit;
        $this->getManager()->execCommand($command['sql'], $command['param']);
        // aggiornamento tabella sorgente (se non verrà tolto il record)
        if (!$isDeleted) {
            $this->update($fromResource, [$conf['traced'] => $vkey], $fromKey, 'from');
        }
        return true;
    }
}
