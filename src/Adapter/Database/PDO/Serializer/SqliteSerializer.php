<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datasource\Adapter\Database\PDO\Serializer;

use Spinit\Datasource\Adapter\Database\PDO\PDOSerializer;
use Spinit\Datasource\Adapter\Database\PDO\Helper\ExceptionValue;
use Spinit\Util;
/**
 * Description of MyswlSerializer
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class SqliteSerializer extends PDOSerializer
{
    protected function serializeBinkey($value, $default) {
        try {
            return parent::serializeBinkey($value, $default);
        } catch (ExceptionValue $e) {
            if (strlen($value)%2) {
                $value = '0'.$value;
            }
            throw new ExceptionValue("x'{$value}'");
        }
    }
}
