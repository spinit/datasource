<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datasource\Adapter\Database;

use Spinit\Datasource\Core\StructInterface;

use function Spinit\Util\arrayGet;

/**
 * Description of Manager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Manager
{
    private $info;
    private $params;
    private $database;
    
    public function __construct($database, $info)
    {
        $this->info = $info;
        $this->params = array();
        $this->database = $database;
    }
    
    public function getInfo()
    {
        return $this->info;
    }
    
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
        return $this;
    }
    public function getParam($name, $default = '')
    {
        return arrayGet($this->params, $name, $default);
    }
    public function getDataBase()
    {
        return $this->database;
    }
    public function getNameDB()
    {
        return '';
    }
    abstract public function load($query, $param = array(), $args = array());
    abstract public function connect();
    abstract public function align(StructInterface $struct);
    abstract public function getResourceInfo($name);
    abstract public function insert($resource, $data);
    abstract public function update($resource, $data, $pkey);
    abstract public function delete($resource, $pkey);
    abstract public function find($resource, $fields, $pkey);
    abstract public function first($query, $params = array(), $args = array());
}
