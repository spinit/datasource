<?php
namespace Spinit\Datasource\Core;

use Spinit\Datasource\Core\QueryInterface;

/**
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface StructInterface
{
    function getName();
    function getFields();
    function getParam($name);
}
