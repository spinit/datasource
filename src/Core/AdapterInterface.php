<?php
/**
 * Interfaccia dell'oggetto che gestisce la configurazione della sorgente dati da utilizzare
 */
namespace Spinit\Datasource\Core;

/**
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

interface AdapterInterface
{
    public function load($query, $param = array(), $args = array());
    public function first($query, $param = array(), $args = array());
    public function align($struct);
    public function find($resource, $fields, $pkey);
    public function insert($resource, $data);
    public function update($resource, $data, $key, $compare = false);
    public function delete($resource, $key);
    public function setSource($source);
    public function getSource();
    public function getName();
}
