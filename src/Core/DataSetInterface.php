<?php
namespace Spinit\Datasource\Core;

use Spinit\Datasource\Core\QueryInterface;

/**
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataSetInterface extends \Iterator
{
    public function isOpen();
    
    /**
     * Chiude la sorgente dati
     */
    public function close();
    
    /**
     * Restiuisce la posizione del record corrente
     */
    public function position();
    
    /**
     * Restituisce le informazioni associate al dataset
     */
    public function getMetadata($type = '');
}
