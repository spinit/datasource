<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource;

use Spinit\Datasource\Core\StructInterface;
use Spinit\Datasource\Core\GetTableStructInterface;

use function Spinit\Util\arrayGet;
/**
 * Description of DataSet
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 * @codeCoverageIgnore
 */
class TableStruct implements StructInterface, GetTableStructInterface
{
    private $struct;
    
    public function __construct($struct = array())
    {
        $this->struct = $struct;
    }
    public function getFields()
    {
        return arrayGet($this->struct, 'fields', array());
    }

    public function getName()
    {
        return arrayGet($this->struct, 'name');
    }
    
    public function getParam($name)
    {
        return arrayGet(arrayGet($this->struct, 'param', array()), $name);
    }

    public function getTableStruct() {
        return $this;
    }

}
