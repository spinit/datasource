<?php

/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource;

use Spinit\Datasource\DataSource;
use Spinit\Datasource\Core\DataSetInterface;
use Spinit\Util\Dictionary;
/**
 * Description of DataSourceSet
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSourceLoad extends DataSource
{
    private $DS;
    private $loadAdapter;
    public function __construct(DataSetInterface $DS, $adapter = null)
    {
        parent::__construct();
        $this->DS = $DS;
        $this->loadAdapter = $adapter;
    }
   
    public function load($query, $param = array(), $args = array())
    {
       return $this->DS;
    }
    public function getName()
    {
        return '-';
    }
    public function getAdapter()
    {
        return $this->loadAdapter;
    }
}
