<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource;

use Spinit\Datasource\Core\QueryInterface;
use Spinit\Datasource\Core\StructInterface;
use Spinit\Datasource\Core\AdapterInterface;

use Spinit\Util;

use Webmozart\Assert\Assert;
use Spinit\UUIDO;

/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSource
{
    /**
     * Lista 
     * @var array
     */
    private static $listAdapter = array();
    
    /**
     * Lista funzioni callaback per la normalizzazione del codice
     * @var type 
     */
    private $listNormalizer = array();
    
    /**
     *
     * @var AdapterInterface
     */
    private $adapter;
    
    /**
     *
     * @var UUIDO\Maker
     */
    private $counter;
    
    public function __construct($connectionString = '')
    {
        if ($connectionString) {
            $this->setAdapter(self::searchAdapter($connectionString));
        }
        $this->setCounter(new UUIDO\Maker(UUIDO\randHex()));
    }
    public function setCounter($counter)
    {
        $this->counter = $counter;
    }
    /**
     * 
     * @return UUIDO\Maker
     */
    public function getCounter()
    {
        return $this->counter;
    }
    public function setAdapter(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $adapter->setSource($this);
    }
    public function getAdapter()
    {
        Assert::notNull($this->adapter, "Adapter non impostato");
        return $this->adapter;
    }
    
    public function load($query, $param = array(), $args = array())
    {
        $dataset = $this->getAdapter()->load($query, $param, $args);
        self::assertIsDataSet($dataset);
        return $dataset;
    }
    
    public function first($query, $param = array(), $args = array())
    {
        return $this->getAdapter()->first($query, $param, $args);
    }
    
    public function align($struct, $mainDS = null)
    {
        return $this->getAdapter()->align($struct, $mainDS);
    }
    
    public function find($resource, $fields, $pkey)
    {
        return $this->getAdapter()->find($resource, $fields, $pkey);
    }
    public function insert($resource, $data)
    {
        return $this->getAdapter()->insert($resource, $data);
    }
    public function update($resource, $data, $key, $compare = false)
    {
        return $this->getAdapter()->update($resource, $data, $key, $compare);
    }
    public function delete($resource, $key)
    {
        return $this->getAdapter()->delete($resource, $key);
    }
    
    /**
     * Interfaccia che permette ad un oggetto esterno di intervenire nella pulizia del codice
     * che dovrà essere eseguito dall'adapter
     * @param type $callback
     */
    public function addNormalizer($callback)
    {
        $this->listNormalizer[] = $callback;
    }
    public function normalize($code)
    {
        foreach($this->listNormalizer as $call)
        {
            Assert::isCallable($call, 'Normalizzatore errato : '.json_encode($call));
            $code = call_user_func($call, $code);
        }
        return $code;
        
    }
    
    public function getName()
    {
        return $this->getAdapter()->getName();
    }
    /**
     * Permette di poter associare un generatore di Adapter ad un protocollo
     * @param type $protocol
     * @param type $classAdapter
     */
    public static function registerAdapter($protocol, $classAdapter)
    {
        foreach (Util\asArray($protocol, ',') as $p) {
            self::$listAdapter[$p] = $classAdapter;
        }
    }
    
    /**
     * Attraverso la prima porzione della stringa di connessione individua la factory che genera l'adapter
     * e ne richiede la generazione
     * 
     * @param type $stringConnection
     */
    public static function searchAdapter($stringConnection)
    {
        Assert::notEmpty($stringConnection, 'Stringa di connessione vuota');
        
        $param = explode(':', $stringConnection);
        $protocol = array_shift($param);
        if (isset(self::$listAdapter[$protocol])) {
            $class = self::$listAdapter[$protocol];
        } else {
            $class = "Spinit\\Datasource\\Adapter\\DatabaseAdapter";
        }
        
        Assert::notEmpty($class);
        
        $adapter = Util\getInstance($class, $stringConnection);
        
        Assert::isInstanceOf($adapter, "Spinit\\Datasource\\Core\\AdapterInterface");
        
        return $adapter;
        
    }
    
    public static function assertIsDataSet($dataset)
    {
        Assert::isInstanceOf($dataset, '\\Spinit\\Datasource\\Core\\DataSetInterface','DataSet di tipo errato : %s');
    }
}
