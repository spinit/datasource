<?php

/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource;

use Spinit\Datasource\Core\DataSetInterface;

/**
 * Description of DatasetArray
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSetArray extends \ArrayIterator implements DataSetInterface
{
    private $open = true;
    private $position = 0;
    
    public function close()
    {
        $this->open = false;
    }

    public function isOpen()
    {
        return $this->open;
    }

    public function position()
    {
        return $this->position;
    }
    
    public function next() {
        if (!$this->isOpen()) {
            return false;
        }
        parent::next();
        $this->position += 1;
        return $this->current();
    }

    public function getMetadata($type = '')
    {
        $info = [];
        if ($this->current()) {
            switch($type) {
                case '':
                case 'col':
                    foreach(array_keys($this->current()) as $col) {
                        $info[$col] = ['name'=>$col, 'label'=>$col, 'type'=> null];
                    }
                    break;
            }
        }
        return $info;
    }
    
    public function getList()
    {
        return $this->getArrayCopy();
    }
}
