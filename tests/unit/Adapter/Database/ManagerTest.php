<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database;

use Spinit\Datasource\Adapter\DatabaseAdapter;
use Spinit\Datasource\TableStruct;

/**
 * Description of ManagerTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testParam()
    {
        $database = new DatabaseAdapter();
        $database->connect('sqlite::memory');
        $manager = $database->getManager();
        $manager->setParam('var','test');
        $this->assertEquals('test', $manager->getParam('var'));
        $this->assertTrue($manager->getPdo() != null);
    }
    
    public function testFind()
    {
        $fields = array (
            'id'=>array('name'=>'id', 'type'=>'binkey', 'pkey'=>'1'),
            'nme'=>array('name'=>'nme', 'type'=>'varchar'),
        );
        $database = new DatabaseAdapter();
        $database->connect('sqlite::memory');
        $database->align(new TableStruct(array('name'=>'ttest', 'fields'=>$fields)));
        $database->insert('ttest', array('id'=>'ff32', 'nme'=>'ciao'));
        
        $dat = $database->find('ttest', array('nme'), array('id'=>'ff32'));
        $this->assertEquals(array('nme'=>'ciao'), $dat);
        $dat = $database->find('ttest', array('nme'), 'f33');
        $this->assertEquals(null, $dat);
    }
}
