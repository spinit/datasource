<?php

/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
namespace Spinit\Datasource\Adapter\Database\PDO\Helper\UnitTest;

use Spinit\Datasource\Adapter\Database\PDO\Helper\Command;
use Spinit\Datasource\Adapter\Database\PDO\Helper\ExceptionValue;

use PHPUnit\Framework\TestCase;
use Spinit\Util;

/**
 * Description of CommandInsertTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CommandTest extends TestCase
{
    public function getResourceInfo($name)
    {
        return [
            'id'=>['type'=>'binary', 'pkey'=>'1'],
            'nme'=>['type'=>'varchar'],
            '__pref_id__'=>['type'=>'binary'],
        ];
    }
    public function getNextValue($type, $size)
    {
        static $value = 1;
        return $value ++;
    }
    public function getRefNameAndValue($type, $name, $value)
    {
        if ($type == 'binary') {
            throw new ExceptionValue("HEX('{$value}')");
        }
        return [':'.$name, $value];
    }
    public function formatColumn($type, $name)
    {
        if ($type == 'binary') {
            throw new ExceptionValue("UNHEX({$name})");
        }
        return $name;
    }
    public function getResourceStruct()
    {
        return array();
    }
    public function first()
    {
        return ['id'=>'test'];
    }
    /**
     * 
     * @var CommandInsert
     */
    private $object;
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $ds = new \Spinit\Datasource\DataSource(TEST_CONNECTION);
        $ds->getAdapter()->getManager()->execQuery("drop table if exists test ");
    }
    public function setUp()
    {
        $ds = new \Spinit\Datasource\DataSource(TEST_CONNECTION);
        $this->object = new Command($ds->getAdapter()->getManager());
        $ds->align(new \Spinit\Datasource\TableStruct(array(
            'name'=>'test',
            'fields'=>[
                'id'=>['type'=>'binary', 'pkey'=>'1'],
                'nme'=>['type'=>'varchar']
                ]
            )));
    }
    
    public function testInsert()
    {
        $pkey = $this->object->insert('test',array('nme' => 'ciao'));
        $this->assertTrue(strlen(array_shift(array_values($pkey))) > 0);
    }
    
    public function testIdValue()
    {
        $actual = $this->object->insert('test',array('nme' => 'test', 'id'=>'abc'));
        $this->assertEquals("abc", $actual['id']);
    }
    
    public function testUpdate()
    {
        $rec = $this->object->find('test', 'id, nme', array('nme'=>'ciao'));
        $this->assertEquals('ciao', $rec['nme']);
        $actual = $this->object->update('test',array('nme' => 'ciao2'), $rec['id']);
        $actual = $this->object->find('test', 'id, nme', $rec['id']);
        $this->assertEquals('ciao2', $actual['nme']);
    }
    
    public function xtestDelete()
    {
        $actual = $this->object->delete('test', array('id'=>'45'));
        $this->assertEquals(2, count($actual));
        $this->assertContains("45", $actual['sql']);
        $actual = $this->object->delete('test', array('nme'=>'ciao'));
        $this->assertContains("nme = :nme", $actual['sql']);
        $this->assertEquals(array('nme'=>'ciao'), $actual['param']);
    }
    
    public function testTrace()
    {
        $rec = $this->object->find('test', 'id, nme', 'abc');
        $traceConf = array(
            'traced' => '__pref_id__',
            'tracing'=>'__pref_rec__',
            'suffix'=>'__',
            'deleting'=>'__is_del__',
            'opr_usr'=>'__usr_opr__',
            'opr_dat'=>'__dat_opr__'
        );
        $actual = $this->object->trace('test', $rec['id'], $traceConf, false);
        $trace = $this->object->getManager()->find('test__','id, nme',array('__pref_rec__' => $rec['id']));
        $this->assertTrue(count($trace) > 0);
        $this->assertTrue(strlen($trace['id']) > 0);
        $this->assertNotEquals($trace['id'], $rec['id']);
        $this->assertEquals($trace['nme'], $rec['nme']);
    }
}
