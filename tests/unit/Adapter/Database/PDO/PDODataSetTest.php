<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Datasource\Adapter\Database\PDO;

/**
 * Description of PDODataSetTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDODataSetTest extends \PHPUnit_Framework_TestCase
{
    private $object;
    
    public function setUp()
    {
        $this->pdo = $pdo = new \PDO('sqlite::memory:');
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $pdo->exec('drop table if exists t');
        $pdo->exec("create table t (id int primary key, cod varchar(10), txt text)");
        $pdo->exec("insert into  t (id, cod) values (1,'uno')");
        $pdo->exec("insert into  t (id, cod, txt) values (2,'due', 'ok')");
        $this->object = new PDODataSet($pdo->query('select id, cod from t'));
    }
    public function tearDown() {
        $this->pdo->exec('drop table if exists t');
    }
    public function testPosition()
    {
        foreach($this->object as $key => $record) {
            $this->assertEquals(0, $this->object->position());
            break;
        }
        $this->object->next();
        $this->assertEquals(array('id'=>2), $this->object->key());
        $this->assertEquals(array('id'=>2, 'cod'=>'due'), $this->object->current());
        $this->object->next();
        $this->assertEquals(null, $this->object->key());
        $this->assertEquals(null, $this->object->current());
    }
    public function testGetAll()
    {
        $data = $this->object->getAll();
        $this->assertEquals(array(
                array('id'=>1, 'cod'=>'uno'),
                array('id'=>2, 'cod'=>'due')
            ),
            $data
        );
    }
    public function testGetList()
    {
        $data = $this->object->getList();
        $this->assertEquals(array(1=>'uno', 2=>'due'), $data);
        $this->object = new PDODataSet($this->pdo->query('select id from t'));
        $data = $this->object->getList();
        $this->assertEquals(array(1, 2), $data);
        $this->object = new PDODataSet($this->pdo->query('select * from t'));
        $data = $this->object->getList();
        $this->assertEquals(array(1 =>['id'=>1,'cod'=>'uno', 'txt'=>''], 2=>['id'=>2,'cod'=>'due', 'txt'=>'ok']), $data);
    }
}
