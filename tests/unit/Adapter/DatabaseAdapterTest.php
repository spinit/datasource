<?php
namespace Spinit\Datasource\Adapter;

use Spinit\Datasource\Testing\Core\Struct;
use Spinit\Datasource\Testing\Core\Query;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DatabaseAdapterTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var PDOAdapter
     */
    private $object;
    
    public function setUp() {
        $this->object = new DatabaseAdapter();
    }
    
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage hello
     * @expectedExceptionCode 100
     */
    public function testRegisterManager()
    {
        DatabaseAdapter::registerManager('test', function($adapter, $arg){
            throw new \Exception($arg[1], 100);
        });
        $this->object->connect('test:hello');
    }
    /**
     * @expectedException \Exception
     */
    public function testConnectEmpty()
    {
        $this->assertTrue($this->object->connect(''));
    }
    /**
     * @expectedException \Exception
     */
    public function testConnectSqliteNotFound()
    {
        $this->object->connect('sqlite:'.__DIR__.'/notFound.sqlite3');
    }
    
    /**
     * @expectedException \Exception
     */
    public function testConnectSqliteEmpty()
    {
        $this->object->connect('sqlite:');
    }
    
    public function testConnectSqliteOk()
    {
        $this->assertTrue($this->object->connect('sqlite:'.__DIR__.'/test.sqlite3') != null);
        $this->assertTrue($this->object->connect('sqlite::memory') != null);
    }
        
    public function testCreateTable()
    {
        $this->object->connect('sqlite::memory:');
        $this->object->getManager()->execCommand('drop table if exists t');
        $this->object->getManager()->execCommand('drop table if exists t_');
        $this->assertFalse($this->object->getResourceInfo('t'));
        
        $struct = array(
            'name'=>'t',
            'param'=>['trace'=>'no', 'timing'=>'no'],
            'fields'=>array(
                'id' => array(
                    'type'=>'bigint',
                    'pkey'=>'1'
                ),
                'cod' => array(
                    'type'=>'varchar',
                    'size'=>'10',
                    'default'=>"hello'"
                ),
                'dat_ins' => array(
                    'type'=>'datetime',
                    'default'=>'now'
                )
            )
        );
        $this->object->align(new Struct($struct));
        $actual = $this->object->getResourceInfo('t');
        $this->assertTrue(is_array($actual));
        $this->assertEquals(array('id', 'cod', 'dat_ins'), array_keys($actual));
        //aggiunta di una colonna
        $this->object->align(new Struct(array('name'=>'t', 'fields'=>array('akey'=>array('type'=>'int')))));
        $actual = $this->object->getResourceInfo('t');
        $this->assertArrayHasKey('akey', $actual);
    }
    
    public function testInsert()
    {
        $this->object->connect('sqlite::memory:');
        $this->object->getManager()->execCommand('drop table if exists t');
        $struct = array(
            'name'=>'tt',
            'fields'=>array(
                'a' => array(
                    'type'=>'bigint',
                    'pkey'=>'1'
                ),
                'b' => array(
                    'type'=>'varchar',
                    'size'=>'10'
                )
            )
        );
        $this->object->align(new Struct($struct));
        
        $this->doTestInsert();
        $this->doTestUpdate();
        $this->doTestDelete();
        $this->object->getManager()->execCommand('drop table if exists tt');
    }
    public function doTestInsert()
    {
        $expected = array('a'=>1, 'b'=>"due");
        $this->object->insert('tt', $expected);
        $actual = $this->object->getManager()->first("select a, b from tt where a = 1");
        $this->assertEquals($expected, $actual);
    }
    public function doTestUpdate()
    {
        $expected = array('a'=>1, 'b'=>"uno");
        $this->object->update('tt', array('b'=>$expected['b']), array('a'=>$expected['a']));
        $actual = $this->object->getManager()->find("tt", array('a','b'), array('a'=>1));
        $this->assertEquals($expected, $actual);
    }
    public function doTestDelete()
    {
        $expected = false;
        $this->object->delete('tt', array('a'=>1));
        $actual = $this->object->getManager()->first("select * from tt where a = 1");
        $this->assertEquals($expected, $actual);
    }
    
    public function testQueryOk()
    {
        $this->object->connect('sqlite::memory:');
        $this->object->getManager()->execCommand('drop table if exists t');
        $struct = array(
            'name'=>'t',
            'fields'=>array(
                'a' => array(
                    'type'=>'bigint',
                    'pkey'=>'1'
                ),
                'b' => array(
                    'type'=>'varchar',
                    'size'=>'10'
                )
            )
        );
        $this->object->align(new Struct($struct));
        $expected = array('a'=>1, 'b'=>'due');
        $this->object->insert('t', $expected);
        
        $dataset = $this->object->load("select a, b from t");
        $this->assertEquals(array($expected), $dataset->getAll());
        
        $dataset = $this->object->load(array('sql'=>"select a, b from t"));
        $this->assertEquals($expected, $dataset->current());
        
        $dataset = $this->object->load(array('sqlite'=>"select a, b from t"));
        $this->assertEquals($expected, $dataset->current());
    }
    
    public function testQueryParameters()
    {
        $this->object->connect('sqlite::memory:');
        $this->object->getManager()->execCommand('drop table if exists t');
        $struct = array(
            'name'=>'t',
            'fields'=>array(
                'a' => array(
                    'type'=>'bigint',
                    'pkey'=>'1'
                ),
                'b' => array(
                    'type'=>'varchar',
                    'size'=>'10'
                )
            )
        );
        $this->object->align(new Struct($struct));
        $expected1 = array('a'=>1, 'b'=>'due');
        $this->object->insert('t', $expected1);
        $expected2 = array('a'=>3, 'b'=>"l'altro");
        $this->object->insert('t', $expected2);
        
        $dataset = $this->object->load("select a, b from t where a = {{id}}", array('id'=>1));
        $this->assertEquals($expected1, $dataset->current());
        
        $dataset = $this->object->load("select a, b from t where b = '{{dsc}}'", array('dsc'=>"l'altro"));
        $this->assertEquals($expected2, $dataset->current());
        
        $dataset = $this->object->load(array('sql'=>"select a, b from t where a = {{id}} and b = :dsc"), array('id'=>3, 'dsc'=>"l'altro"));
        $this->assertEquals($expected2, $dataset->current());
        
    }
    /**
     * Testing query
     * @expectedException \Exception
     * @expectedExceptionMessage Query not found
     */
    public function testQueryNotFound()
    {
        $this->object->connect('sqlite::memory:');
        $this->object->getManager()->execCommand('drop table if exists t');
        $struct = array(
            'name'=>'t',
            'fields'=>array(
                'a' => array(
                    'type'=>'bigint',
                    'pkey'=>'1'
                ),
                'b' => array(
                    'type'=>'varchar',
                    'size'=>'10'
                )
            )
        );
        $this->object->align(new Struct($struct));
        $expected = array('a'=>1, 'b'=>'due');
        $this->object->insert('t', $expected);
    
        $dataset = $this->object->load(array('mysql'=>"select * from t"));
        $dataset->next();
        $this->assertEquals($expected, $dataset->current());
    }
}