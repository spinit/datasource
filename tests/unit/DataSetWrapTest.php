<?php
namespace Spinit\Datasource\UnitTest;

use Spinit\Datasource\DataSetWrap;

class DataSetWrapTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var DatasetArray
     */
    private $object;
    
    public function testCurrent()
    {
        $record = array("uno", "due");
        $wrap = array('current' => function() use ($record) {return $record;});
        $this->object = new DataSetWrap($wrap);
        $this->assertEquals($record, $this->object->current());
        $this->assertEquals('', $this->object->key());
        $this->assertEquals(0, $this->object->position());
    }
    
    public function testEmpty()
    {
        $this->object = new DataSetWrap(array());
        $this->assertEquals(false, $this->object->valid());
        $this->assertEquals(false, $this->object->rewind());
        $this->assertEquals(false, $this->object->next());
    }
    
    public function testNext()
    {
        $this->object = new DataSetWrap(array('next'=>function(){return array('1');}));
        $this->assertEquals(array('1'), $this->object->next());
        $this->assertEquals(1, $this->object->position());
        $this->object->close();
        $this->assertEquals(false, $this->object->next());
        $this->assertEquals(1, $this->object->position());
    }
}
