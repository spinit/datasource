<?php
namespace Spinit\Datasource;

use Spinit\Datasource\Core\AdapterInterface;

use Spinit\Datasource\Testing\Core\Query;
use Spinit\Datasource\Testing\Core\DataSet;
use Spinit\Datasource\Testing\Core\Struct;

use Spinit\Datasource\Adapter\DatabaseAdapter;

class DataSourceTest extends \PHPUnit_Framework_TestCase implements AdapterInterface
{
    /**
     *
     * @var Datasource
     */
    private $object;
    
    /**
     * 
     * @param type $resource
     * @param StructInterface $struct
     */
    public function align($struct, $mainDS = null) {
        return $struct->getName().' aligned';
    }

    public function delete($resource, $key) {
        return $resource . ' deleted';
    }

    public function insert($resource, $data) {
        return $resource . ' inserted';
    }

    public function load($query, $param = array(), $args = array()) {
        return $query['dataset'];
    }

    public function update($resource, $data, $key, $compare = false) {
        return $resource . ' updated';
    }

    /****
     * 
     */
    public function setUp()
    {
        DataSource::registerAdapter('test', function ($stringConnection) {
            return $this;
        });
        $this->object = new DataSource('test:hello world');
        $this->object->setAdapter($this);
    }
    public function testAdapter()
    {
        $this->assertInstanceOf('\\Spinit\\Datasource\\Core\\AdapterInterface', $this->object->getAdapter());
        $this->assertEquals($this, $this->object->getAdapter());
    }
    public function testQueryOk()
    {
        $query = array('dataset'=>new DataSet());
        $this->assertTrue($this->object->load($query) != null);
    }
    
    /**
     * @expectedException \Exception
     */
    public function testQueryBad()
    {
        $this->object->load(null);
    }
    
    public function testAlign()
    {
        $this->assertEquals('OK aligned', $this->object->align(new Struct(array('name'=>'OK'))));
    }
    public function testInsert()
    {
        $this->assertEquals('OK inserted', $this->object->insert('OK', array()));
    }
    public function testDelete()
    {
        $this->assertEquals('OK deleted', $this->object->delete('OK', array()));
    }
    public function testUpdate()
    {
        $this->assertEquals('OK updated', $this->object->update('OK', array(), array()));
    }
    
    public function testDatabaseAdapter()
    {
        $ds =  new DataSource('sqlite::memory:');
        $this->assertInstanceOf("Spinit\\Datasource\\Adapter\\DatabaseAdapter", $ds->getAdapter());
    }

    public function find($resource, $fields, $pkey) {
        
    }

    public function first($query, $param = array(), $args = array()) {
        
    }

    public function getSource() {
        return $this->source;
    }

    public function setSource($source) {
        $this->source = $source;
    }

}
