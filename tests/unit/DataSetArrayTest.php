<?php
namespace Spinit\Datasource;


class DataSetArrayTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var DatasetArray
     */
    private $object;
    
    public function testAdapter()
    {
        $this->object = new DataSetArray(array(array("uno", "due"), array("tre", "quattro"), array("cinque", "sei")));
        $this->assertEquals(array("uno", "due"), $this->object->current());
        $this->assertEquals(0, $this->object->position());
        $this->assertEquals(array("tre", "quattro"), $this->object->next());
        $this->object->close();
        $this->assertFalse($this->object->next());
    }
    public function testMetaData()
    {
        $this->object = new DataSetArray(array(array("uno"=>"due", "tre"=>"quattro", "cinque"=>"sei")));
        $this->assertEquals(array("uno", "tre", "cinque"), array_keys($this->object->getMetadata()));
    }
}
