<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include(dirname(__DIR__).'/vendor/autoload.php');

// determinazione stringa di connessione
if (getenv('CONNECTION')) {
    define('TEST_CONNECTION', getenv('CONNECTION'));
} else if (getenv('MYSQL_DATABASE')) {
    $user = getenv('MYSQL_USER') ? getenv('MYSQL_USER') : 'root';
    $passwd = getenv('MYSQL_USER') ? (getenv('MYSQL_PASSWORD') ? getenv('MYSQL_PASSWORD') : getenv('MYSQL_ROOT_PASSWORD')) : getenv('MYSQL_ROOT_PASSWORD');
    define('TEST_CONNECTION', 'mysql:'.getenv('MYSQL_HOST').':'.getenv('MYSQL_DATABASE').':'.$user.':'.$passwd);
    
} else {
    define('TEST_CONNECTION', 'sqlite::memory');
    //define('TEST_CONNECTION','sqlite:/home/ermanno/test.sqlite');
    //define('TEST_CONNECTION', 'mysql:develop:test:test:test ');
}

echo "\n>> String connection : ".TEST_CONNECTION."\n\n";
