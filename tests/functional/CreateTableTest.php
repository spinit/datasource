<?php

/*
 * Copyright (C) 2018 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
use PHPUnit\Framework\TestCase;
use Spinit\Datasource\DataSource;
use Spinit\Datasource\Testing\Core\Struct;
use Spinit\Util;

/**
 * Description of CreateTableTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CreateTableTest extends TestCase
{
    private $DS;
    
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->DS = new DataSource(TEST_CONNECTION);
        Util\getenv('USER_LOGGED_ID', 1);
    }
    
    public function xtestSimpleTable()
    {
        try {
            $this->DS->getAdapter()->getManager()->execCommand('DROP TABLE stable');
            $this->DS->getAdapter()->getManager()->execCommand('DROP TABLE stable__');
        } catch (\Exception $e) {
            
        }
        $struct = new Struct(array('name'=>'stable', 'fields' => array(
            'id'=>array('type'=>'binkey', 'pkey'=>'1'),
            'nme'=>array('type'=>'varchar')
        )));
        $this->DS->align($struct);
        
        // insert
        $pkey = $this->DS->insert('stable', array('nme'=>'hello'));
        $data = $this->DS->load("select * from stable")->getAll();
        $this->assertEquals(1, count($data));
        $this->assertEquals('hello', $data[0]['nme']);
        $vkey = strtoupper(bin2hex($data[0]['id']));
        $this->assertTrue(strlen($vkey) > 0);
        $this->assertEquals(['id'=>$vkey], $pkey);
        $this->assertEquals(null, $data[0]['__pref_id__']);
        $this->assertEquals('01', bin2hex($data[0]['usr_ins']));
        
        // update
        $this->DS->update('stable', array('nme'=>'ciao'), $pkey);
        $item = $this->DS->find('stable', 'id, nme, __pref_id__', $pkey);
        $trace = $this->DS->find('stable__', 'id, nme', ['__pref_rec__'=>$pkey['id']]);
        
        $this->assertEquals($item['__pref_id__'], $trace['id']);
        $this->assertEquals('ciao', $item['nme']);
        $this->assertEquals('hello', $trace['nme']);
        $this->assertNotEquals($trace['id'], $item['id']);
    }
    
    public function testListTable()
    {
        try {
            $this->DS->getAdapter()->getManager()->execCommand('DROP TABLE ltable');
            $this->DS->getAdapter()->getManager()->execCommand('DROP TABLE ltable__');
            $this->DS->getAdapter()->getManager()->execCommand('DROP TABLE ltable__L');
        } catch (\Exception $e) {
            
        }
        $struct = new Struct(array('name'=>'ltable', 'fields' => array(
            'id'=>array('type'=>'binkey', 'pkey'=>'1'),
            'nme'=>array('type'=>'list', 'conf'=> 'str')
        )));        
        $this->DS->align($struct);
        
        return;
        // inserimento
        $pkey = str_repeat('12345678', 4);
        $this->DS->insert('ltable', array('id'=>$pkey, 'nme'=>array([null, 'hello'])));
        $actual = $this->DS->find("ltable", array('id', 'nme'), $pkey);
        var_dump($actual['nme'], [null, 'hello']);exit;
        $this->assertEquals($pkey, $actual['id']);
        $this->assertArraySubset(array([null, 'hello']), $actual['nme']);
        
        //modifica
        $actual['nme'][0][1] = 'ciao';
        $this->DS->update('ltable', $actual, $pkey);
        $item = $this->DS->find("ltable", array('id', 'nme'), $pkey);
        $this->assertEquals($item, $actual);
        $this->assertArraySubset(array([null, 'ciao']), $item['nme']);
    }
}
