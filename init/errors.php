<?php
namespace Spinit\Datasource\Error;

class DataSourceException extends \Exception {
    
}
class TypeNotFoundException extends DataSourceException {
    
}