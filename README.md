# Coverage
[![Build status](https://gitlab.com/spinit/datasource/badges/master/build.svg)](https://gitlab.com/spinit/datasource/commits/master)
[![Overall test coverage](https://gitlab.com/spinit/datasource/badges/master/coverage.svg)](https://spinit.gitlab.io/datasource/)

Libreria per astrarre la gestione dei dati dalla particolare sorgente.